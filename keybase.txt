==================================================================
https://keybase.io/naereen
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://lbesson.bitbucket.io
  * I am naereen (https://keybase.io/naereen) on keybase.
  * I have a public key ASB0qooyut1iVIMM35DaR2wUiYQ-q4jMDNUPL_LGM_0DTwo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "012074aa8a32badd6254830cdf90da476c1489843eab88cc0cd50f2ff2c633fd034f0a",
      "host": "keybase.io",
      "kid": "012074aa8a32badd6254830cdf90da476c1489843eab88cc0cd50f2ff2c633fd034f0a",
      "uid": "961eddd112157a366d636b1bb19cd819",
      "username": "naereen"
    },
    "merkle_root": {
      "ctime": 1589042943,
      "hash": "9b365429db0494f9a93b32f33a0c82593f2f3292606267e4e6dec163b852a3bd4367dadbab23167d542cf3d73422293c48262d66046ceec1888493643cd7819c",
      "hash_meta": "afa95116cad62b082027d81986f54e44b946369d4c298cb8528b7adc1fe7414a",
      "seqno": 16232351
    },
    "service": {
      "entropy": "+3f75oS7+JNWahI4DECVFy8h",
      "hostname": "lbesson.bitbucket.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.4.2"
  },
  "ctime": 1589042993,
  "expire_in": 504576000,
  "prev": "1d6944246a7c20b50802fd18c122daff71a161aaa8a2d917bb0472cab54a584a",
  "seqno": 7,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgdKqKMrrdYlSDDN+Q2kdsFImEPquIzAzVDy/yxjP9A08Kp3BheWxvYWTESpcCB8QgHWlEJGp8ILUIAv0YwSLa/3GhYaqootkXuwRyyrVKWErEIGdzRIeiW22+9f7wLp4uF1B467NVzOj8fqGQe/1MNCiyAgHCo3NpZ8RA4wc7MJSce2L8z2fUwEd1qLoQUg39i75CS7DGBIklwp8jX30QpQiYxxAstX37SXhQE0+Di2paXL0UWEnTp3EbDahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIHyda1BoWXJSKmwFFjAqHwa2DpKW/cJ0mGSE5O4bmis1o3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/naereen

==================================================================
