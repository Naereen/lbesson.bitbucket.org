<!DOCTYPE html>
<!-- © and (C) Lilian Besson, 2013 : for //lbesson.bitbucket.io/phpinfo.php -->
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>phpinfo.php</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="phpinfo.php" />
    <meta name="author" content="Lilian Besson" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes" />

    <link rel="shortcut icon" type="image/x-icon" href="http://perso.crans.org/besson/_static/.favicon.ico?1" />
    <link rel="stylesheet" type="text/css" href="http://perso.crans.org/besson/_static/nprogress.css?1" />
    <link rel="stylesheet" type="text/css" href="http://perso.crans.org/besson/_static/forkit.css?1" />

    <meta name="google-site-verification" content="wU18kCoTfbukrhACrsDyaSBfKRFaVErblritzOhsBOU" />
</head>
<body>
    <script type="text/javascript" src="http://perso.crans.org/besson/_static/jquery.js?1"></script>
    <script type="text/javascript" src="http://perso.crans.org/besson/_static/nprogress.js?1"></script>
    <script async type="text/javascript" src="http://perso.crans.org/besson/_static/ga.js?1"></script>
    <script type="text/javascript">NProgress.start();</script>
    <div>
      <style type="text/css">
        .forkit {
          top: 0;
        }
      </style>
      <a class="forkit" data-text="Sur BitBucket ?" data-text-detached="Tout est OpenSource !" href="http://bitbucket.org/lbesson">
      <img style="position: absolute; top: 0; right: 0; border: 0;" alt="Consultez mes dépôts git!" /></a>
    </div>

    <div id="loading">Chargement en cours...</div>

    <div id="test">
  		<?php
  			phpinfo();
  		?>
    </div>	

<footer>
  <script async type="text/javascript" src="http://perso.crans.org/besson/_static/forkit.js?1"></script>
  <script type="text/javascript">
    NProgress.inc();
    $(document).ready(function(){
        (function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,"script","//www.google-analytics.com/analytics.js","ga");
        ga("create", "UA-38514290-14", "bitbucket.org");
        ga("send", "pageview");
        console.log( "[GA] Sending with ID=UA-38514290-14, DOMAIN=bitbucket.org" );
        setTimeout(function() {
          NProgress.done();
          $("#loading").text("");
        }, 1000);
      });
  </script>
</footer>
<img style="visibility: hidden; display: none;" src="https://ga-beacon.appspot.com/UA-38514290-2/phpinfo.php?pixel"/>
</body>
</html>
